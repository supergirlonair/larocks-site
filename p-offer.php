<?php

// Payload which is sent to server
$payload = [
    'apikey' => 'LKrUbr1zK78kgtMpXSKOmL3BF2llCizh',
    'nonce' => time(),
];

// Generation of apiseal
$apiseal = hash_hmac('sha256', http_build_query($payload), 'PTaZLdsodQLorlw3uhTf0Yx7srUCV_Hx');

// Append the generated apiseal to payload
$payload['apiseal'] = $apiseal;

// Set request URL (in this case we check your balance)
$ch = curl_init('https://paxful.com/api/offer/all');

// NOTICE that we send the payload as a string instead of POST parameters
curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($payload));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Accept: application/json; version=1',
    'Content-Type: text/plain',
]);

// fetch response
$response = curl_exec($ch);

// convert json response into array
$data = json_decode($response);

var_dump($data);

curl_close($ch);

?>